import pytz

from rest_framework import serializers

from django.db import transaction

from calendarapp.models import Attendance, Calendar, \
    CalendarPermission, Event


class AttendanceSerializer(serializers.ModelSerializer):

    user_email = serializers.CharField(source='user.email', read_only=True)

    def validate(self, data):
        """ Check that user is not invited yet. """
        if data.get('event', None):
            invitation_exists = Attendance.objects.filter(
                event=data['event'],
                user=data['user'],
            ).exists()
            if invitation_exists:
                raise serializers.ValidationError('User already invited')
        return data

    class Meta:
        model = Attendance


class CalendarSerializer(serializers.ModelSerializer):

    is_owner = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField()

    def get_request_user(self):
        return self.context['request'].user

    def get_is_owner(self, obj):
        return obj.owner_id == self.get_request_user().id

    def get_permissions(self, obj):
        """
        If user is not an owner of the calendar, retrieve
        his permissions.
        """
        permissions = []
        user_id = self.get_request_user().id
        if obj.owner_id != user_id:
            permissions = obj.permissions.filter(
                user_id=user_id).values_list(
                    'permission_type', flat=True)
        return permissions

    def save(self, **kwargs):
        """
        Always sets request user as owner of the calendar.
        """
        self.validated_data['owner'] = self.get_request_user()
        return super(CalendarSerializer, self).save(**kwargs)

    class Meta:
        model = Calendar


class CalendarPermissionSerializer(serializers.ModelSerializer):

    user_email = serializers.CharField(source='user.email', read_only=True)

    def validate_user(self, value):
        """ Check if user doesn't want to grant permission to himself. """
        if value == self.context['request'].user:
            raise serializers.ValidationError('User is already an owner')
        return value

    def validate(self, data):
        """ Check that user has no such permission yet. """
        permission_exists = CalendarPermission.objects.filter(
            calendar=data['calendar'],
            user=data['user'],
            permission_type=data['permission_type']
        ).exists()
        if permission_exists:
            raise serializers.ValidationError('User already has such permission')
        return data

    class Meta:
        model = CalendarPermission


class EventSerializer(serializers.ModelSerializer):

    has_write_access = serializers.SerializerMethodField()
    user_attendance = serializers.SerializerMethodField()

    calendar_error_message = 'Object does not exist or you have no permission to add events'

    def __init__(self, *args, **kwargs):
        super(EventSerializer, self).__init__(*args, **kwargs)
        # Add the same message in case of DoesNotExist exception and
        # in case user has no write permission to given calendar
        self.fields['calendar'].error_messages['does_not_exist'] = self.calendar_error_message

    def get_request_user_id(self):
        return self.context['request'].user.id

    def create(self, validated_data):
        with transaction.atomic():
            event = super(EventSerializer, self).create(validated_data)
            # Owner always attends his event
            Attendance.objects.create(event=event, user_id=event.calendar.owner_id,
                                      attendance=Attendance.YES)
        return event

    def _get_has_write_access(self, calendar):
        user_id = self.get_request_user_id()
        if calendar.owner_id == user_id:
            return True
        permission_exists = CalendarPermission.objects.filter(
            calendar_id=calendar.id,
            user_id=user_id,
            permission_type=CalendarPermission.WRITE
        ).exists()
        if permission_exists:
            return True
        return False

    def get_has_write_access(self, obj):
        return self._get_has_write_access(obj.calendar)

    def get_user_attendance(self, obj):
        try:
            attendance = obj.attendance.get(
                user_id=self.get_request_user_id())
        except Attendance.DoesNotExist:
            return None
        serializer = AttendanceSerializer(attendance)
        return serializer.data

    def validate_calendar(self, calendar):
        if not self._get_has_write_access(calendar):
            raise serializers.ValidationError(self.calendar_error_message)
        return calendar

    def validate(self, data):
        """ Check if correct timezone and dates have been provided """
        start_date = data.get('start_date', None)
        end_date = data.get('end_date', None)
        if not data.get('all_day', False):
            if data.get('timezone', None):
                try:
                    pytz.timezone(data['timezone'])
                except pytz.UnknownTimeZoneError:
                    raise serializers.ValidationError('Wrong timezone info')
            elif not self.instance or not self.instance.timezone:
                raise serializers.ValidationError('No timezone provided')
        # TODO: Check with current instance,
        # optionally check if for all day theres from 0.00 to 0.00
        if start_date and end_date and not data['start_date'] < data['end_date']:
            raise serializers.ValidationError('Wrong date range')
        return data

    class Meta:
        model = Event
