from mock import patch

from rest_framework import serializers, status
from rest_framework.test import APIClient, APITestCase

from calendarapp.models import Attendance, Calendar, CalendarPermission
from calendarapp.serializers import EventSerializer


# Unit tests

class EventSerializerTest(APITestCase):

    fixtures = [
        'calendarapp/test_fixtures/users.json',
        'calendarapp/test_fixtures/calendars.json',
        'calendarapp/test_fixtures/calendar_permissions.json',
        'calendarapp/test_fixtures/events.json',
        'calendarapp/test_fixtures/attendances.json',
    ]

    def setUp(self):
        self.serializer = EventSerializer()

    def test__get_has_write_access(self):
        # owner
        calendar_1 = Calendar.objects.get(id=1)
        # write permission
        calendar_2 = Calendar.objects.get(id=2)
        # no permission
        calendar_3 = Calendar.objects.get(id=3)
        # read permission
        calendar_4 = Calendar.objects.get(id=4)
        with patch.object(EventSerializer, 'get_request_user_id', return_value=1):
            result_1 = self.serializer._get_has_write_access(calendar_1)
            result_2 = self.serializer._get_has_write_access(calendar_2)
            result_3 = self.serializer._get_has_write_access(calendar_3)
            result_4 = self.serializer._get_has_write_access(calendar_4)
            self.assertTrue(result_1, 1)
            self.assertTrue(result_2, 1)
            self.assertFalse(result_3, 1)
            self.assertFalse(result_4, 1)

    def test_validate_wrong_dates(self):
        data = {
            'start_date': '2016-01-01 12:00:00',
            'end_date': '2016-01-01 11:00:00',
            'all_day': True,
        }
        self.assertRaises(serializers.ValidationError,
                          self.serializer.validate,
                          data)

    def test_validate_wrong_timezone(self):
        data = {
            'timezone': 'Aaaaa',
        }
        self.assertRaises(serializers.ValidationError,
                          self.serializer.validate,
                          data)

    def test_validate_no_timezone(self):
        data = {}
        self.assertRaises(serializers.ValidationError,
                          self.serializer.validate,
                          data)

    def test_validate_correct(self):
        data = {
            'start_date': '2016-01-01 12:00:00',
            'end_date': '2016-01-01 13:00:00',
            'timezone': 'Europe/Warsaw',
        }
        result = self.serializer.validate(data)
        self.assertEqual(data, result)


# Integration tests

class CalendarViewsTest(APITestCase):

    fixtures = [
        'calendarapp/test_fixtures/users.json',
        'calendarapp/test_fixtures/calendars.json',
        'calendarapp/test_fixtures/calendar_permissions.json',
        'calendarapp/test_fixtures/events.json',
        'calendarapp/test_fixtures/attendances.json',
    ]

    def setUp(self):
        self.client = APIClient()
        self.client.login(username='test', password='test')

    def test_add_calendar(self):
        response = self.client.post('/api/calendars/',
                                    {'name': 'new test calendar', 'owner': 2})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Calendar was assigned to an owner
        self.assertEqual(response.data.get('owner', None), 1)

    def test_retrieve_calendar(self):
        response = self.client.get('/api/calendars/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_calendar_no_permission(self):
        self.client.login(username='test2', password='test')
        response = self.client.get('/api/calendars/1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_calendar_read_permission(self):
        self.client.login(username='test1', password='test')
        response = self.client.get('/api/calendars/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_edit_calendar(self):
        new_name = 'test calendar 1 - new name'
        response = self.client.patch('/api/calendars/1/',
                                     {'name': new_name, 'owner': 2})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Can't change owner to someone else
        self.assertEqual(response.data.get('owner', None), 1)
        self.assertEqual(response.data.get('name', None), new_name)

    def test_edit_calendar_no_permission(self):
        self.client.login(username='test2', password='test')
        new_name = 'test calendar 1 - new name'
        response = self.client.patch('/api/calendars/1/',
                                     {'name': new_name, 'owner': 2})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_edit_calendar_not_owner(self):
        self.client.login(username='test1', password='test')
        new_name = 'test calendar 1 - new name'
        response = self.client.patch('/api/calendars/1/',
                                     {'name': new_name, 'owner': 2})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_calendar(self):
        response = self.client.delete('/api/calendars/1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_calendar_no_permission(self):
        self.client.login(username='test2', password='test')
        response = self.client.delete('/api/calendars/1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_calendar_not_owner(self):
        self.client.login(username='test1', password='test')
        response = self.client.delete('/api/calendars/1/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_list_calendars(self):
        response = self.client.get('/api/calendars/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # 1 calendar user owns and one shared
        self.assertEqual(len(response.data), 3)
        ids = [calendar.get('id', None) for calendar in response.data]
        # User is only invited to an event from the calendar
        self.assertNotIn(3, ids)

    def test_share(self):
        response = self.client.post('/api/calendars/1/share/',
                                    {'user': 2, 'permission_type': 2})
        permissions_count = CalendarPermission.objects.filter(
            calendar_id=1, user_id=2,
            permission_type=CalendarPermission.WRITE).count()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(permissions_count, 1)

    def test_share_no_permission(self):
        self.client.login(username='test2', password='test')
        response = self.client.post('/api/calendars/1/share/',
                                    {'user': 2, 'permission_type': 2})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_share_not_owner(self):
        self.client.login(username='test1', password='test')
        response = self.client.post('/api/calendars/1/share/',
                                    {'user': 2, 'permission_type': 2})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_unshare(self):
        response = self.client.delete('/api/calendars/1/unshare/',
                                      {'id': 2})
        permissions_count = CalendarPermission.objects.filter(
            calendar_id=1, user_id=2,
            permission_type=CalendarPermission.READ).count()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(permissions_count, 0)

    def test_unshare_no_permission(self):
        self.client.login(username='test2', password='test')
        response = self.client.delete('/api/calendars/1/unshare/',
                                      {'id': 2})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unshare_not_owner(self):
        self.client.login(username='test1', password='test')
        response = self.client.delete('/api/calendars/1/unshare/',
                                      {'id': 2})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_shared_to(self):
        response = self.client.get('/api/calendars/1/shared_to/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_shared_to_no_permission(self):
        self.client.login(username='test2', password='test')
        response = self.client.get('/api/calendars/1/shared_to/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_shared_to_not_owner(self):
        self.client.login(username='test1', password='test')
        response = self.client.get('/api/calendars/1/shared_to/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class AddEventsTest(APITestCase):

    fixtures = [
        'calendarapp/test_fixtures/users.json',
        'calendarapp/test_fixtures/calendars.json',
        'calendarapp/test_fixtures/calendar_permissions.json',
        'calendarapp/test_fixtures/events.json',
        'calendarapp/test_fixtures/attendances.json',
    ]

    def setUp(self):
        self.client = APIClient()
        self.client.login(username='test', password='test')

    def test_add_event(self):
        data = {
            'calendar': 1,
            'title': 'event_title',
            'start_date': '2016-01-01 00:00:00',
            'end_date': '2016-01-01 01:10:00',
            'timezone': 'Europe/Warsaw',
        }
        response = self.client.post('/api/events/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        attendance_created = Attendance.objects.filter(
            event_id=response.data.get('id', None),
            user_id=1).exists()
        self.assertTrue(attendance_created)
        data.pop('timezone')
        data['all_day'] = True
        response = self.client.post('/api/events/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_add_event_wrong_dates(self):
        data = {
            'calendar': 1,
            'title': 'event_title',
            'start_date': '2016-01-01 02:00:00',
            'end_date': '2016-01-01 01:10:00',
            'timezone': 'Europe/Warsaw',
        }
        response = self.client.post('/api/events/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_event_wrong_timezone(self):
        data = {
            'calendar': 1,
            'title': 'event_title',
            'start_date': '2016-01-01 00:00:00',
            'end_date': '2016-01-01 01:10:00',
            'timezone': 'Europe1/Warsaw',
        }
        response = self.client.post('/api/events/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_event_no_permission(self):
        data = {
            'calendar': 3,
            'title': 'event_title',
            'start_date': '2016-01-01 00:00:00',
            'end_date': '2016-01-01 01:10:00',
            'timezone': 'Europe/Warsaw',
        }
        response = self.client.post('/api/events/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_event_no_write_permission(self):
        self.client.login(username='test1', password='test')
        data = {
            'calendar': 1,
            'title': 'event_title',
            'start_date': '2016-01-01 00:00:00',
            'end_date': '2016-01-01 01:10:00',
            'timezone': 'Europe/Warsaw',
        }
        response = self.client.post('/api/events/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data.get('calendar', None),
                         [EventSerializer.calendar_error_message])

    def test_add_event_write_permission(self):
        data = {
            'calendar': 2,
            'title': 'event_title',
            'start_date': '2016-01-01 00:00:00',
            'end_date': '2016-01-01 01:10:00',
            'timezone': 'Europe/Warsaw',
        }
        response = self.client.post('/api/events/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class DeleteEventsTest(APITestCase):

    fixtures = [
        'calendarapp/test_fixtures/users.json',
        'calendarapp/test_fixtures/calendars.json',
        'calendarapp/test_fixtures/calendar_permissions.json',
        'calendarapp/test_fixtures/events.json',
        'calendarapp/test_fixtures/attendances.json',
    ]

    def setUp(self):
        self.client = APIClient()
        self.client.login(username='test', password='test')

    def test_delete_event(self):
        response = self.client.delete('/api/events/2/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_event_no_permission(self):
        response = self.client.delete('/api/events/1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_event_no_write_permission(self):
        self.client.login(username='test1', password='test')
        response = self.client.delete('/api/events/2/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_event_write_permission(self):
        response = self.client.delete('/api/events/3/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class RetrieveEventsTest(APITestCase):

    fixtures = [
        'calendarapp/test_fixtures/users.json',
        'calendarapp/test_fixtures/calendars.json',
        'calendarapp/test_fixtures/calendar_permissions.json',
        'calendarapp/test_fixtures/events.json',
        'calendarapp/test_fixtures/attendances.json',
    ]

    def setUp(self):
        self.client = APIClient()
        self.client.login(username='test', password='test')

    def test_retrieve_event(self):
        # as guest
        response = self.client.get('/api/events/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # as owner
        response = self.client.get('/api/events/5/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_event_no_permission(self):
        self.client.login(username='test2', password='test')
        response = self.client.get('/api/calendars/1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_event_read_permission(self):
        self.client.login(username='test1', password='test')
        response = self.client.get('/api/events/2/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class EventsTest(APITestCase):

    fixtures = [
        'calendarapp/test_fixtures/users.json',
        'calendarapp/test_fixtures/calendars.json',
        'calendarapp/test_fixtures/calendar_permissions.json',
        'calendarapp/test_fixtures/events.json',
        'calendarapp/test_fixtures/attendances.json',
    ]

    def setUp(self):
        self.client = APIClient()
        self.client.login(username='test', password='test')

    def test_list_events(self):
        response = self.client.get('/api/events/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # as guest and writer
        self.assertEqual(len(response.data), 4)

    def test_clone_event(self):
        response = self.client.post('/api/events/1/clone/', {'calendar': 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class InviteEventTest(APITestCase):

    fixtures = [
        'calendarapp/test_fixtures/users.json',
        'calendarapp/test_fixtures/calendars.json',
        'calendarapp/test_fixtures/calendar_permissions.json',
        'calendarapp/test_fixtures/events.json',
        'calendarapp/test_fixtures/attendances.json',
    ]

    def setUp(self):
        self.client = APIClient()
        self.client.login(username='test', password='test')

    def test_invite(self):
        response = self.client.post('/api/events/2/invite/', {'user': 2})
        invitation_exists = Attendance.objects.filter(
            event_id=2, user_id=2, attendance=Attendance.UNKNOWN).exists()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(invitation_exists)

    def test_invite_no_permission(self):
        response = self.client.post('/api/events/1/invite/', {'user': 2})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invite_no_write_permission(self):
        self.client.login(username='test1', password='test')
        response = self.client.post('/api/events/2/invite/', {'user': 2})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_invite_write_permission(self):
        response = self.client.post('/api/events/3/invite/', {'user': 2})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_uninvite(self):
        self.client.login(username='test1', password='test')
        response = self.client.delete('/api/events/1/uninvite/', {'id': 1})
        invitation_exists = Attendance.objects.filter(
            event_id=1, user_id=1).exists()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(invitation_exists)

    def test_uninvite_no_permission(self):
        response = self.client.delete('/api/events/1/uninvite/', {'id': 1})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_uninvite_no_write_permission(self):
        self.client.login(username='test1', password='test')
        response = self.client.delete('/api/events/2/uninvite/', {'id': 2})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_uninvite_write_permission(self):
        response = self.client.delete('/api/events/3/uninvite/', {'id': 3})
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invited_list(self):
        # as guest
        response = self.client.get('/api/events/1/invited_list/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # as owner
        response = self.client.get('/api/events/2/invited_list/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        # as reader
        self.client.login(username='test1', password='test')
        response = self.client.get('/api/events/2/invited_list/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)


class EditEventsTest(APITestCase):

    fixtures = [
        'calendarapp/test_fixtures/users.json',
        'calendarapp/test_fixtures/calendars.json',
        'calendarapp/test_fixtures/calendar_permissions.json',
        'calendarapp/test_fixtures/events.json',
        'calendarapp/test_fixtures/attendances.json',
    ]

    def setUp(self):
        self.client = APIClient()
        self.client.login(username='test', password='test')

    def test_edit_event_no_permission(self):
        data = {
            'title': 'event_title',
        }
        response = self.client.patch('/api/events/1/', data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_edit_event_no_write_permission(self):
        self.client.login(username='test1', password='test')
        data = {
            'title': 'event_title',
        }
        response = self.client.patch('/api/events/2/', data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_edit_event_write_permission(self):
        new_event_title = 'new event title'
        response = self.client.patch('/api/events/3/', {'title': new_event_title})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('title'), new_event_title)


class ManageAttendanceTest(APITestCase):

    fixtures = [
        'calendarapp/test_fixtures/users.json',
        'calendarapp/test_fixtures/calendars.json',
        'calendarapp/test_fixtures/calendar_permissions.json',
        'calendarapp/test_fixtures/events.json',
        'calendarapp/test_fixtures/attendances.json',
    ]

    def setUp(self):
        self.client = APIClient()
        self.client.login(username='test', password='test')

    def test_answer_invitation(self):
        data = {
            'attendance': Attendance.NO,
            'event': 2,
        }
        response = self.client.patch('/api/attendances/1/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Can't change event
        self.assertEqual(response.data.get('event', None), 1)
        self.assertEqual(response.data.get('attendance', None), Attendance.NO)

    def test_answer_invitation_no_permission(self):
        self.client.login(username='test1', password='test')
        data = {
            'attendance': Attendance.NO,
            'event': 2,
        }
        response = self.client.patch('/api/attendances/1/', data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_invitation(self):
        response = self.client.delete('/api/attendances/1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_invitation_no_permission(self):
        self.client.login(username='test1', password='test')
        response = self.client.delete('/api/attendances/1/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
