from rest_framework.routers import DefaultRouter

from django.conf.urls import url, include

from calendarapp.models import Attendance, CalendarPermission
from calendarapp.views import *


router = DefaultRouter()
router.register(r'attendances', AttendanceViewSet)
router.register(r'calendars', CalendarViewSet)
router.register(r'events', EventViewSet)

urlpatterns = [
    url(r'', include(router.urls)),

    url(r'^calendar_permission_type/$',
        DictionaryView.as_view(
            choices=CalendarPermission.PERMISSION_CHOICES)),
    url(r'^attendance_type/$',
        DictionaryView.as_view(choices=Attendance.ATTENDANCE_CHOICES)),
]