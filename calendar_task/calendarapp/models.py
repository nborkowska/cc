from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models


class Attendance(models.Model):

    YES = 'yes'
    NO = 'no'
    MAYBE = 'maybe'
    UNKNOWN = 'unknown'

    ATTENDANCE_CHOICES = (
        (YES, 'Yes'),
        (NO, 'No'),
        (MAYBE, 'Maybe'),
        (UNKNOWN, 'Unknown'),
    )

    event = models.ForeignKey('Event', related_name='attendance')
    user = models.ForeignKey(User)
    attendance = models.CharField(max_length=15,
                                  choices=ATTENDANCE_CHOICES)


class Calendar(models.Model):

    owner = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    # html color code
    color = models.CharField(max_length=7, blank=True,
                             validators=[RegexValidator(
                                 regex='^#[a-zA-Z0-9]{6}$')])


class CalendarPermission(models.Model):

    READ = 1
    WRITE = 2

    PERMISSION_CHOICES = (
        (READ, 'Read'),
        (WRITE, 'Write'),
    )

    calendar = models.ForeignKey(Calendar, related_name='permissions')
    user = models.ForeignKey(User)
    permission_type = models.IntegerField(choices=PERMISSION_CHOICES)


class Event(models.Model):

    calendar = models.ForeignKey(Calendar)
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    timezone = models.CharField(max_length=50, blank=True)
    all_day = models.BooleanField(default=False)