from rest_framework import filters, mixins, status
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import Http404
from django.shortcuts import get_object_or_404

from calendarapp.models import Attendance, Calendar, CalendarPermission, Event
from calendarapp.serializers import AttendanceSerializer, \
    CalendarPermissionSerializer, CalendarSerializer, EventSerializer


class AttendanceViewSet(mixins.DestroyModelMixin,
                        mixins.ListModelMixin,
                        mixins.RetrieveModelMixin,
                        mixins.UpdateModelMixin,
                        GenericViewSet):
    """ This viewset allows to manage users invitations.
    """

    queryset = Attendance.objects.all()
    serializer_class = AttendanceSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        queryset = super(AttendanceViewSet, self).get_queryset()
        return queryset.filter(user_id=self.request.user.id)

    def update(self, request, *args, **kwargs):
        """ Allows user to answer event invitation """
        # Allow to change invitation answer only, ignore all other request data
        data = {'attendance': request.data.get('attendance', None)}
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class WritableMixin(object):
    """ Provides update() and destroy() methods in cases you want to ensure
    user has writing permission to an object he wants to update/destroy. """

    def get_writable_object(self):
        """ This should be a modification of get_object()
        method."""
        raise NotImplementedError

    def update(self, request, *args, **kwargs):
        """ Just like update() method from mixins.UpdateModelMixin,
        but calls get_writable_object() instead of get_object(). """
        partial = kwargs.pop('partial', False)
        instance = self.get_writable_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        """ Just like destroy() method from mixins.DestroyModelMixin,
        but calls get_writable_object() instead of get_object() """
        instance = self.get_writable_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class CalendarViewSet(WritableMixin, ModelViewSet):

    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer
    permission_serializer_class = CalendarPermissionSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        """ Takes all calendars user is allowed to see - as owner
        or as permission holder. """
        user_id = self.request.user.id
        queryset = super(CalendarViewSet, self).get_queryset()
        queryset = queryset.filter(
            Q(owner_id=user_id)|
            Q(permissions__user_id=user_id)).distinct()
        return queryset

    def get_writable_object(self):
        """ Get calendar by its pk, but only in case user owns it. """
        obj = get_object_or_404(Calendar, id=self.kwargs['pk'])
        user_id = self.request.user.id
        if obj.owner_id != user_id:
            if obj.permissions.filter(user_id=user_id).exists():
                # If user already knows sth's there, return 403
                raise PermissionDenied
            else:
                raise Http404
        return obj

    @detail_route(methods=['post'])
    def share(self, request, *args, **kwargs):
        instance = self.get_writable_object()
        data = request.data.copy()
        data['calendar'] = instance.id
        context = {'request': request}
        serializer = self.permission_serializer_class(
            data=data, context=context)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @detail_route(methods=['delete'])
    def unshare(self, request, *args, **kwargs):
        """ Requires permission's id. """
        instance = self.get_writable_object()
        permission_id = request.data.get('id', None)
        objs_deleted, deletions = CalendarPermission.objects.filter(
            id=permission_id, calendar_id=instance.id).delete()
        if not objs_deleted:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @detail_route(methods=['get'])
    def shared_to(self, request, *args, **kwargs):
        """ Get list of permissions of users
        with whom calendar has been shared. """
        instance = self.get_writable_object()
        serializer = self.permission_serializer_class(
            instance.permissions.all(), many=True)
        return Response(serializer.data)


class DictionaryView(APIView):
    """ Provides valid choices for model fields. """

    permission_classes = (IsAuthenticated, )
    choices = ()

    def get(self, request, *args, **kwargs):
        data = [{
            'id': i[0],
            'name': i[1]
        } for i in self.choices]
        return Response(data)


class EventViewSet(WritableMixin, ModelViewSet):

    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (IsAuthenticated, )
    attendance_serializer_class = AttendanceSerializer
    filter_backends = (filters.DjangoFilterBackend, )
    filter_fields = ('calendar', )

    def get_queryset(self):
        """ Get all events user can see because he's a guest, owner
        or has at least READ permission to event's calendar. """
        user_id = self.request.user.id
        queryset = super(EventViewSet, self).get_queryset()
        queryset = queryset.filter(
            Q(attendance__user_id=user_id) |
            Q(calendar__owner_id=user_id) |
            Q(calendar__permissions__user_id=user_id,
              calendar__permissions__permission_type__gte=\
                  CalendarPermission.READ)).distinct()
        return queryset

    def get_writable_object(self):
        """ Get event you can write to. """
        obj = get_object_or_404(Event, id=self.kwargs['pk'])
        user_id = self.request.user.id
        if obj.calendar.owner_id != user_id:
            # Check if user has any permissions to event's calendar
            permissions = obj.calendar.permissions.filter(
                user_id=user_id).values_list('permission_type', flat=True)
            if not permissions:
                raise Http404
            elif CalendarPermission.WRITE in permissions:
                # User is not an owner, but he has write permission to event's calendar
                return obj
            else:
                # If user already knows sth's there, return 403
                raise PermissionDenied
        return obj

    @detail_route(methods=['get'])
    def invited_list(self, request, *args, **kwargs):
        """ Get list of invited guests"""
        instance = self.get_object()
        serializer = self.attendance_serializer_class(
            instance.attendance.all(), many=True)
        return Response(serializer.data)

    @detail_route(methods=['post'])
    def clone(self, request, *args, **kwargs):
        """ If user wants to edit an event he is invited to,
        local event copy is made. """
        parent = self.get_object()
        # Do not copy objects id nor calendar id, as we want to
        # clone an event to users own calendar.
        exclude_fields = ['id', 'calendar']
        data = {'calendar': request.data.get('calendar', None)}
        # Copy all other fields
        for field in parent._meta.fields:
            name = field.name
            if name not in exclude_fields:
                data[name] = getattr(parent, name)
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @detail_route(methods=['post'])
    def invite(self, request, *args, **kwargs):
        instance = self.get_writable_object()
        data = request.data.copy()
        data['event'] = instance.id
        data['attendance'] = Attendance.UNKNOWN
        serializer = self.attendance_serializer_class(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @detail_route(methods=['delete'])
    def uninvite(self, request, *args, **kwargs):
        """ Requires invitation's id. """
        instance = self.get_writable_object()
        id = request.data.get('id', None)
        objs_deleted, deletions = Attendance.objects.filter(
            id=id, event_id=instance.id).delete()
        if not objs_deleted:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_204_NO_CONTENT)
