Notes:

1. Normally I do FK's to dictionary type objects like attendance type (YES/NO/MAYBE/UNKNOWN), because they are easy to extend later. In this case there would be no other options.
2. I assume frontend will provide me timezone in string format from IANA db. Both pytz and jstimezonedetect js library use that. Js can detect users timezone and put is as default in event form.
3. If user tries to perform an action he has no permission to, app returns 404 status or 403, depending on an user. If user tries to ex. delete other users calendar, but he has read access to it, it returns 403. If user picks random calendar id (so he doesn't know that resource exists), app returns 404 for security reasons. 
4. If user wants to edit an event he is invited to, he can clone it to his calendar.
5. I made two types of tests: unit and integration tests.
6. In my 'integration' tests I tried to check all major cases.

Available urls (sorry it's not a swagger library):

/api/login/ methods: POST

Login

/api/logout/  methods: POST

Logout

/api/attendances/ methods: GET

List all user's invitations / attendances (for request.user)

/api/attendances/(?P<pk>[^/.]+)/ methods: GET, PATCH, PUT, DELETE

Get attendance details, update and delete (invitation owner only)

/api/calendars/ methods: POST

Create new calendar

/api/calendars/(?P<pk>[^/.]+)/ methods: PATCH, PUT, DELETE

Update or delete calendar (owner only)

/api/calendars/(?P<pk>[^/.]+)/share/ methods: POST

Share calendar (owner only)

/api/calendars/(?P<pk>[^/.]+)/unshare/ methods: DELETE

Unshare (owner only)

/api/calendars/(?P<pk>[^/.]+)/shared_to/ methods: GET

List users calendar has been shared to (owner only)

/api/calendars/ methods: GET

List all available calendars (owner or people with whom calendars are shared)

/api/calendars/(?P<pk>[^/.]+)/ methods: GET

Get calendar details (owner or people with whom calendar has been shared). You will also see if you have write access to it.

/api/events/ methods: GET

Get all available events (your own, from shared calendar, the ones you are invited to)

/api/events/(?P<pk>[^/.]+)/ methods: GET

Get event details (your own, from shared calendar, the ones you are invited to). You will also see if you can edit an event.

/api/events/(?P<pk>[^/.]+)/clone/ methods: POST

Copy event to your calendar ('edit' event you are invited to)

/api/events/(?P<pk>[^/.]+)/invited_to/ methods: GET

List all users invited to the event (owner, guest, people with whom calendar has been shared)

/api/events/ methods: POST

Create new event (owner, people who have WRITE permission)

/api/events/(?P<pk>[^/.]+)/ methods: PUT, PATCH, DELETE

Update and delete an event (owner, people who have WRITE permission)

/api/events/(?P<pk>[^/.]+)/invite/ methods: POST

Invite user to an event (owner, people who have WRITE permission)

/api/events/(?P<pk>[^/.]+)/uninvite/ methods: DELETE

Uninvite user (owner, people who have WRITE permission)

/api/calendar_permission_type/ methods: GET

Get all available calendar permission types

/api/attendance_type/ methods: GET

Get all available attendance types
